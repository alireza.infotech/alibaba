import axios from "axios";

export default {
    state: () => ({
        hotels: []
    }),

    actions: {
        fetchAllHotels({commit}) {

            axios.get(`http://localhost:4000/apiv1/hotel`)
                .then(response => {
                    console.log(response.data)
                    commit('setAllHotels', response.data)
                })
        }
    },

    mutations: {
        setAllHotels(state, hotels) {
            state.hotels = hotels;
        }
    },

    getters: {
        getAllHotels(state) {
            return state.hotels;
        }
    }
}